
addon.name      = 'formation'
addon.author    = 'bambooya'
addon.version   = '0.1'
addon.desc      = 'position multiple characters in static/moving formations (just for fun)'
addon.link      = 'https://gitlab.com/ffxi_multibox/ashita/addons/formation'

require('common')

require('multibox_common')

local formation = T{}
local config = T{}

local function default_settings()
    local config = T{}

    config.log_level = 'info'
    config.log_console_level = 'warn'

    config = config:merge(slots.default_settings())

    return config
end

ashita.events.register('load', 'layout_load_callback', function ()
    config = settings.load(default_settings(), 'settings', true)
    log.level = config.log_level
    log.console_level = config.log_console_level
    log.info('loading')
    slots.set_config(config.slot)
end)

ashita.events.register('unload', 'layout_unload_callback', function ()
    log.info('unloading')
    settings.save('settings')
end)

-- function pre-declarations
local formation_begin
local formation_end

-- settings
local min_dist = 0.01
local offset_back = 1.25
local timeout = 5
local min_scale = 1
local max_scale = 10

-- globals (within the scope of this file)
local current_formation = nil
local leader_facing = nil
local leader_pos = nil
local pos_idx = nil
local current_slot = nil
local leader_slot = nil
local scale = 1
local end_time = nil


ashita.events.register('command', 'formation_command_callback', function (e)
    local args = e.command:args();
    local command = table.remove(args, 1)
    local formation = table.remove(args, 1)

    -- formation v|^|-|||o|<o|.o|.<o|.-|.c|.<c|.v|oo|o-o|vv|pw [scale (1..10)]
    if command == '/formation' or command == '/form' then
        local self = ffxi.get_mob_by_target('me')
        if self == nil then return end

        local current_slot = slots.get_current_window().slot
        if current_slot == nil then return end

        if formation == 'end' or formation == 'stop' or formation == nil then
            local message = 'formation end '..tostring(current_slot)
            ipc.send_message(message, 'all')
        else
            -- handle optional scale arg
            local scale = 1
            if args[1] ~= nil then
                scale_arg = tonumber(args[1])
                if scale_arg >= min_scale and scale_arg <= max_scale then
                    scale = scale_arg
                end
            end

            local message = 'formation '..formation..' '..tostring(current_slot)..' '..tostring(self.facing)..' '..tostring(self.x)..' '..tostring(self.y)..' '..tostring(scale)
            ipc.send_message(message, 'all')
        end
    end
end)

ipc.register(function(args)
    local command = table.remove(args, 1)

    if command == 'formation' then
        if args[1] == 'leadstate' then
            leader_facing = tonumber(args[2])
            leader_pos = {x=tonumber(args[3]), y=tonumber(args[4])}
        elseif args[1] == 'end' or args[1] == 'stop' or args[1] == current_formation then
            -- formation end|stop <leader_slot>
            formation_end()
        else
            -- formation <formation_string_spec> <leader_slot> <facing> <x> <y> <scale>
            formation_begin(args[1], tonumber(args[2]), tonumber(args[3]), tonumber(args[4]), tonumber(args[5]), tonumber(args[6]))
        end
    end
end)

formation_begin = function(form, lslot, face, x, y, s)
    current_slot = slots.get_current_window().slot
    leader_slot = lslot
    if current_slot == nil or leader_slot == nil then return end

    commands.send_command(1, '/shadow follow off')

    current_formation = form
    leader_facing = face
    leader_pos = {x=x, y=y}
    pos_idx = 1
    scale = s
    if current_formation ~= 'oo' and
    current_formation ~= 'o-o' and
    current_formation ~= 'vv' and
    current_formation ~= 'pw' then
        end_time = utils.time() + timeout
    else
        end_time = nil
    end
end

formation_end = function(id)
    current_formation = nil
    leader_slot = nil
    leader_facing = nil
    leader_pos = nil
    pos_idx = nil
    scale = 1
    end_time = nil
    ffxi.run(false)
end

-- convert angle (radians) to vec
local function to_vec(angle)
    x = math.cos(angle)
    y = -math.sin(angle)
    return {x=x, y=y}
end

-- scale .* vec
local function vec_scale(vec, scale)
    return {x=vec.x*scale, y=vec.y*scale}
end

-- vec1 + vec2
local function vec_add(vec1, vec2)
    return {x=vec1.x+vec2.x, y=vec1.y+vec2.y}
end

-- vec1 - vec2
local function vec_sub(vec1, vec2)
    return {x=vec1.x-vec2.x, y=vec1.y-vec2.y}
end

-- vec ./ norm(vec)
local function vec_norm(vec)
    local norm = math.sqrt(vec.x*vec.x + vec.y*vec.y)
    return {x=vec.x/norm, y=vec.y/norm}
end

-- rotate vec by angle (radians)
local function vec_rotate(vec, angle)
    cosa = math.cos(angle)
    sina = math.sin(angle)
    return {x=vec.x*cosa-vec.y*sina, y=vec.x*sina+vec.y*cosa}
end

local function distanceSquared(A, B)
    local dx = B.x-A.x
    local dy = B.y-A.y
    return dx*dx + dy*dy
end

-- called iteratively until character runs to the formation position specified in the args as follows:
-- leader_pos: absolute position of leader
-- leader_facing: absolute facing direction/orientation of leader
-- follower_offset: relative offset of follower position in formation coordinate space (where positive x is to the right of leader and positive y is in front of leader)
-- follower_facing: relative facing of follower in formation coordinate space (where an angle of 0 radians is facing the same direction as leader and angle increases going counter clockwise when looking from above)
local function move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    local angle_offset = math.pi - leader_facing + math.pi/2.0 -- account for difference between formation (positive x,y to right,front of leader respectively) and global (positive x,y to east,south respectively) coordinate systems
    follower_offset = vec_rotate(follower_offset, angle_offset)
    local follower_pos = vec_add(leader_pos, follower_offset)
    local pos = {follower_pos}
    local pos_next = pos[pos_idx]
    local distSq = distanceSquared(self, pos_next)
    if distSq > min_dist then
        ffxi.run((pos_next.x - self.x), (pos_next.y - self.y))
    else
        pos_idx = pos_idx + 1
        if pos_idx > #pos then
            formation_end()
            ashita.tasks.once(0.5, function() ffxi.turn(leader_facing - follower_facing) end)
        end
    end
end

-- called iteratively until character runs to the formation position specified in the args as follows:
-- leader_pos: absolute position of leader
-- leader_facing: absolute facing direction/orientation of leader
-- follower_offset: relative offset of follower position in formation coordinate space (where positive x is to the right of leader and positive y is in front of leader)
-- follower_facing: relative facing of follower in formation coordinate space (where an angle of 0 radians is facing the same direction as leader and angle increases going counter clockwise when looking from above)
local function move_to_moving_offset(self, leader_pos, leader_facing, follower_offset)
    if leader_facing ~= nil then
        local angle_offset = math.pi - leader_facing + math.pi/2.0 -- account for difference between formation (positive x,y to right,front of leader respectively) and global (positive x,y to east,south respectively) coordinate systems
        follower_offset = vec_rotate(follower_offset, angle_offset)
    end
    local follower_pos = vec_add(leader_pos, follower_offset)
    local distSq = distanceSquared(self, follower_pos)
    if distSq > min_dist then
        ffxi.run((follower_pos.x - self.x), (follower_pos.y - self.y))
    else
        ffxi.run(false)
    end
end

local function formation_v_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-1*scale, y=-1*scale}, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-0.5*scale, y=-0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0.5*scale, y=0*scale}, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=1*scale, y=-0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=1.5*scale, y=-1*scale}, follower_facing)
    end
end

local function formation_v_inv_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-1*scale, y=1*scale}, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-0.5*scale, y=0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0.5*scale, y=0*scale}, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=1*scale, y=0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=1.5*scale, y=1*scale}, follower_facing)
    end
end

local function formation_hline_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-1*scale, y=0}, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-0.5*scale, y=0}, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0.5*scale, y=0}, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=1*scale, y=0}, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=1.5*scale, y=0}, follower_facing)
    end
end

local function formation_vline_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0, y=-0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0, y=-1*scale}, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0, y=-1.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0, y=-2*scale}, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0, y=-2.5*scale}, follower_facing)
    end
end

local function formation_circle_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    local delta_angle = 2.0 * math.pi / 6.0
    local forward_vec = {x=0, y=1*scale}
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        local follower_offset = vec_rotate(forward_vec, delta_angle)
        follower_offset.y = follower_offset.y - 1*scale
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        local follower_offset = vec_rotate(forward_vec, 2 * delta_angle)
        follower_offset.y = follower_offset.y - 1*scale
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local follower_offset = vec_rotate(forward_vec, 3 * delta_angle)
        follower_offset.y = follower_offset.y - 1*scale
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        local follower_offset = vec_rotate(forward_vec, 4 * delta_angle)
        follower_offset.y = follower_offset.y - 1*scale
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local follower_offset = vec_rotate(forward_vec, 5 * delta_angle)
        follower_offset.y = follower_offset.y - 1*scale
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    end
end

local function formation_circle_all_facing_inward(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local delta_angle = 2.0 * math.pi / 6.0
    local forward_vec = {x=0, y=1*scale}
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        local angle = delta_angle + math.pi
        local follower_offset = vec_rotate(forward_vec, angle)
        follower_offset.y = follower_offset.y + 1*scale
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        local angle = 2 * delta_angle + math.pi
        local follower_offset = vec_rotate(forward_vec, angle)
        follower_offset.y = follower_offset.y + 1*scale
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local angle = 3 * delta_angle + math.pi
        local follower_offset = vec_rotate(forward_vec, angle)
        follower_offset.y = follower_offset.y + 1*scale
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        local angle = 4 * delta_angle + math.pi
        local follower_offset = vec_rotate(forward_vec, angle)
        follower_offset.y = follower_offset.y + 1*scale
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local angle = 5 * delta_angle + math.pi
        local follower_offset = vec_rotate(forward_vec, angle)
        follower_offset.y = follower_offset.y + 1*scale
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    end
end

local function formation_circle(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    local delta_angle = 2.0 * math.pi / 5.0
    local forward_vec = {x=0, y=1*scale}
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        local angle = delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        local angle = 2 * delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local angle = 3 * delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        local angle = 4 * delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local angle = 5 * delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    end
end

local function formation_circle_facing_inward(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local delta_angle = 2.0 * math.pi / 5.0
    local forward_vec = {x=0, y=1*scale}
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        local angle = delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        local angle = 2 * delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local angle = 3 * delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        local angle = 4 * delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local angle = 5 * delta_angle
        local follower_offset = vec_rotate(forward_vec, angle)
        local follower_facing = angle + math.pi
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    end
end

local function formation_hline(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-1*scale, y=0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-0.5*scale, y=0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0*scale, y=0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0.5*scale, y=0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=1*scale, y=0.5*scale}, follower_facing)
    end
end

local function formation_half_circle(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    local delta_angle = math.pi / 4.0
    local left_vec = {x=-1*scale, y=0}
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        local angle = 0
        local follower_offset = vec_rotate(left_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        local angle = -delta_angle
        local follower_offset = vec_rotate(left_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local angle = -2 * delta_angle
        local follower_offset = vec_rotate(left_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        local angle = -3 * delta_angle
        local follower_offset = vec_rotate(left_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local angle = -4 * delta_angle
        local follower_offset = vec_rotate(left_vec, angle)
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    end
end

local function formation_half_circle_facing_inward(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local delta_angle = math.pi / 4.0
    local left_vec = {x=-1*scale, y=0}
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        local angle = 0
        local follower_offset = vec_rotate(left_vec, angle)
        local follower_facing = angle - math.pi / 2.0
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        local angle = -delta_angle
        local follower_offset = vec_rotate(left_vec, angle)
        local follower_facing = angle - math.pi / 2.0
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local angle = -2 * delta_angle
        local follower_offset = vec_rotate(left_vec, angle)
        local follower_facing = angle - math.pi / 2.0
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        local angle = -3 * delta_angle
        local follower_offset = vec_rotate(left_vec, angle)
        local follower_facing = angle - math.pi / 2.0
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local angle = -4 * delta_angle
        local follower_offset = vec_rotate(left_vec, angle)
        local follower_facing = angle - math.pi / 2.0
        move_to_offset(self, leader_pos, leader_facing, follower_offset, follower_facing)
    end
end

local function formation_v(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local follower_facing = 0
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-1*scale, y=0*scale}, follower_facing)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=-0.5*scale, y=0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0*scale, y=1*scale}, follower_facing)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=0.5*scale, y=0.5*scale}, follower_facing)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        move_to_offset(self, leader_pos, leader_facing, {x=1*scale, y=0*scale}, follower_facing)
    end
end

local function formation_flying_v(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot - 2 - 1) % 6 + 1 then
        local follower_offset = {x=-1*scale, y=-1*scale}
        move_to_moving_offset(self, leader_pos, leader_facing, follower_offset)
    elseif current_slot == (leader_slot - 1 - 1) % 6 + 1 then
        local follower_offset = {x=-0.5*scale, y=-0.5*scale}
        move_to_moving_offset(self, leader_pos, leader_facing, follower_offset)
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local follower_offset = {x=0*scale, y=0*scale}
        move_to_moving_offset(self, leader_pos, leader_facing, follower_offset)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        local follower_offset = {x=0.5*scale, y=-0.5*scale}
        move_to_moving_offset(self, leader_pos, leader_facing, follower_offset)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local follower_offset = {x=1*scale, y=-1*scale}
        move_to_moving_offset(self, leader_pos, leader_facing, follower_offset)
    end
end

local function formation_moving_circle(current_slot, leader_slot, leader_pos, leader_facing, opposite, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local delta_angle = math.pi / 16.0
    local current_offset = {x=self.x - leader_pos.x, y=self.y - leader_pos.y}
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local follower_offset = vec_rotate(current_offset, delta_angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 2*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        if opposite then delta_angle = -delta_angle end
        local follower_offset = vec_rotate(current_offset, delta_angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 2.75*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local follower_offset = vec_rotate(current_offset, delta_angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 3.5*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    elseif current_slot == (leader_slot + 4 - 1) % 6 + 1 then
        if opposite then delta_angle = -delta_angle end
        local follower_offset = vec_rotate(current_offset, delta_angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 4.25*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    elseif current_slot == (leader_slot + 5 - 1) % 6 + 1 then
        local follower_offset = vec_rotate(current_offset, delta_angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 5*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    end
end

local function formation_pinwheel(current_slot, leader_slot, leader_pos, leader_facing, scale)
    local self = ffxi.get_mob_by_target('me')
    if self == nil then return end

    local delta_angle = (2 * math.pi * (utils.time() / 6))
    local starting_vec = {x=1, y=0}
    local current_offset = {x=self.x - leader_pos.x, y=self.y - leader_pos.y}
    if current_slot == leader_slot then
        -- stay put / do nothing
    elseif current_slot == (leader_slot + 1 - 1) % 6 + 1 then
        local idx = (current_slot > leader_slot) and current_slot - 1 or current_slot
        local angle =  (2 * math.pi / 5 * idx + delta_angle) % (2 * math.pi)
        local follower_offset = vec_rotate(starting_vec, angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 2*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    elseif current_slot == (leader_slot + 2 - 1) % 6 + 1 then
        local idx = (current_slot > leader_slot) and current_slot - 1 or current_slot
        local angle = (2 * math.pi / 5 * idx + delta_angle) % (2 * math.pi)
        local follower_offset = vec_rotate(starting_vec, angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 2*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    elseif current_slot == (leader_slot + 3 - 1) % 6 + 1 then
        local idx = (current_slot > leader_slot) and current_slot - 1 or current_slot
        local angle =  (2 * math.pi / 5 * idx + delta_angle) % (2 * math.pi)
        local follower_offset = vec_rotate(starting_vec, angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 2*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    elseif current_slot == (leader_slot + 4 - 1) % 6 + 1 then
        local idx = (current_slot > leader_slot) and current_slot - 1 or current_slot
        local angle =  (2 * math.pi / 5 * idx + delta_angle) % (2 * math.pi)
        local follower_offset = vec_rotate(starting_vec, angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 2*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    elseif current_slot == (leader_slot + 5 - 1) % 6 + 1 then
        local idx = (current_slot > leader_slot) and current_slot - 1 or current_slot
        local angle =  (2 * math.pi / 5 * idx + delta_angle) % (2 * math.pi)
        local follower_offset = vec_rotate(starting_vec, angle)
        follower_offset = vec_scale(vec_norm(follower_offset), 2*scale)
        move_to_moving_offset(self, leader_pos, nil, follower_offset)
    end
end

ashita.events.register('d3d_beginscene', 'formation_beginscene_callback', function ()
    if leader_slot ~= nil and slots.get_current_window().slot == leader_slot then
        local self = ffxi.get_mob_by_target('me')
        if self ~= nil then
            local message = 'formation leadstate '..self.facing..' '..self.x..' '..self.y
            ipc.send_message(message, 'all')
        end
    end
    if end_time ~= nil and utils.time() > end_time then
        formation_end()
    end
    if current_formation ~= nil then
        if current_formation == 'v' then
            formation_v_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '^' then
            formation_v_inv_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '-' then
            formation_hline_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '|' then
            formation_vline_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == 'o' then
            formation_circle_all(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '<o' then
            formation_circle_all_facing_inward(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '.o' then
            formation_circle(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '.<o' then
            formation_circle_facing_inward(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '.-' then
            formation_hline(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '.c' then
            formation_half_circle(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '.<c' then
            formation_half_circle_facing_inward(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == '.v' then
            formation_v(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == 'oo' then
            formation_moving_circle(current_slot, leader_slot, leader_pos, leader_facing, false, scale)
        elseif current_formation == 'o-o' then
            formation_moving_circle(current_slot, leader_slot, leader_pos, leader_facing, true, scale)
        elseif current_formation == 'vv' then
            formation_flying_v(current_slot, leader_slot, leader_pos, leader_facing, scale)
        elseif current_formation == 'pw' then
            formation_pinwheel(current_slot, leader_slot, leader_pos, leader_facing, scale)
        end
    end
end)
